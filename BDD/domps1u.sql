-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 17 Janvier 2018 à 18:09
-- Version du serveur :  5.1.73
-- Version de PHP :  7.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `domps1u`
--

-- --------------------------------------------------------

--
-- Structure de la table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `liste_id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `descr` text,
  `img` text,
  `url` text,
  `tarif` decimal(5,2) DEFAULT NULL,
  `nomReserveur` varchar(50) DEFAULT NULL,
  `message` text,
  `id_reserveur` int(11) DEFAULT NULL,
  `aCagnotte` double NOT NULL,
  `montant` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

--
-- Contenu de la table `item`
--

INSERT INTO `item` (`id`, `liste_id`, `nom`, `descr`, `img`, `url`, `tarif`, `nomReserveur`, `message`, `id_reserveur`, `aCagnotte`, `montant`) VALUES
(1, 2, 'Champagne', 'Bouteille de champagne + flutes + jeux à gratter', 'champagne.jpg', '', '20.00', NULL, NULL, NULL, 0, 0),
(2, 2, 'Musique', 'Partitions de piano à 4 mains', 'musique.jpg', '', '25.00', NULL, NULL, NULL, 0, 0),
(3, 2, 'Exposition', 'Visite guidée de l’exposition ‘REGARDER’ à la galerie Poirel', 'poirelregarder.jpg', '', '14.00', NULL, NULL, NULL, 0, 0),
(4, 3, 'Goûter', 'Goûter au FIFNL', 'gouter.jpg', '', '20.00', NULL, NULL, NULL, 0, 0),
(5, 3, 'Projection', 'Projection courts-métrages au FIFNL', 'film.jpg', '', '10.00', NULL, NULL, NULL, 0, 0),
(6, 2, 'Bouquet', 'Bouquet de roses et Mots de Marion Renaud', 'rose.jpg', '', '16.00', NULL, NULL, NULL, 0, 0),
(7, 2, 'Diner Stanislas', 'Diner à La Table du Bon Roi Stanislas (Apéritif /Entrée / Plat / Vin / Dessert / Café / Digestif)', 'bonroi.jpg', '', '60.00', NULL, NULL, NULL, 0, 0),
(8, 3, 'Origami', 'Baguettes magiques en Origami en buvant un thé', 'origami.jpg', '', '12.00', NULL, NULL, NULL, 0, 0),
(9, 3, 'Livres', 'Livre bricolage avec petits-enfants + Roman', 'bricolage.jpg', '', '24.00', NULL, NULL, NULL, 0, 0),
(10, 2, 'Diner  Grand Rue ', 'Diner au Grand’Ru(e) (Apéritif / Entrée / Plat / Vin / Dessert / Café)', 'grandrue.jpg', '', '59.00', NULL, NULL, NULL, 0, 0),
(11, 0, 'Visite guidée', 'Visite guidée personnalisée de Saint-Epvre jusqu’à Stanislas', 'place.jpg', '', '11.00', NULL, NULL, NULL, 0, 0),
(12, 2, 'Bijoux', 'Bijoux de manteau + Sous-verre pochette de disque + Lait après-soleil', 'bijoux.jpg', '', '29.00', NULL, NULL, NULL, 0, 0),
(19, 0, 'Jeu contacts', 'Jeu pour échange de contacts', 'contact.png', '', '5.00', NULL, NULL, NULL, 0, 0),
(22, 0, 'Concert', 'Un concert à Nancy', 'concert.jpg', '', '17.00', NULL, NULL, NULL, 0, 0),
(23, 1, 'Appart Hotel', 'Appart’hôtel Coeur de Ville, en plein centre-ville', 'apparthotel.jpg', '', '56.00', NULL, NULL, NULL, 0, 0),
(24, 2, 'Hôtel d''Haussonville', 'Hôtel d''Haussonville, au coeur de la Vieille ville à deux pas de la place Stanislas', 'hotel_haussonville_logo.jpg', '', '169.00', NULL, NULL, NULL, 0, 0),
(25, 1, 'Boite de nuit', 'Discothèque, Boîte tendance avec des soirées à thème & DJ invités', 'boitedenuit.jpg', '', '32.00', NULL, NULL, NULL, 0, 0),
(26, 1, 'Planètes Laser', 'Laser game : Gilet électronique et pistolet laser comme matériel, vous voilà équipé.', 'laser.jpg', '', '15.00', NULL, NULL, NULL, 0, 0),
(27, 1, 'Fort Aventure', 'Découvrez Fort Aventure à Bainville-sur-Madon, un site Accropierre unique en Lorraine ! Des Parcours Acrobatiques pour petits et grands, Jeu Mission Aventure, Crypte de Crapahute, Tyrolienne, Saut à l''élastique inversé, Toboggan géant... et bien plus encore.', 'fort.jpg', '', '25.00', NULL, NULL, NULL, 0, 0),
(28, 5, 'Le chapeau du big boss', 'un chapeau', NULL, '', '500.00', NULL, NULL, NULL, 1, 2),
(56, 6, 'azeza', 'azezaeza', '', '', '4.00', NULL, NULL, NULL, 1, 50),
(57, 7, 'dango', 'un truc tout rond', 'f81545a449c6a697033caa6f21a41933.jpeg', '', '500.00', NULL, NULL, NULL, 1, 500),
(59, 8, 'fgfdjgh', 'fgndgbdhsf', '', '', '54.00', 'clement', 'bonjour', NULL, 0, 0),
(60, 8, 'COucou', 'test', '', '', '54.20', NULL, NULL, NULL, 1, 54.2),
(61, 8, 'ofidgjd', 'ogfdkjfndg', '', '', '54.00', 'scded', 'dedz', NULL, 0, 0),
(62, 10, 'Un livre de php', 'php pour les nuls', 'e01ae17bf772d42026933f01a489a88d.jpeg', '', '40.00', NULL, NULL, NULL, 1, 40),
(63, 10, 'Un téléphone portable ', 'samsung!', '', 'http://www.samsung.com/fr/', '200.00', 'Jean paul', 'joyeux noel', NULL, 0, 0),
(64, 8, 'test', 'ttttt', '', 'https://www.google.fr/search?q=dango&rlz=1C1GCEA_enFR780FR780&oq=dango&aqs=chrome..69i57j0l5.1055j0j7&sourceid=chrome&ie=UTF-8', '15.30', NULL, NULL, NULL, 0, 0),
(65, 9, 'Une table', 'pour manger', 'eb0a331e7e1e04a5d2e217d51c165975.jpeg', '', '500.50', 'Jean', 'Bon anniv', NULL, 0, 0),
(66, 9, 'Une brouette', 'Je veux jardiner', 'bcf2170730dbd9af670eab1c9a1add5b.jpeg', '', '50.90', NULL, NULL, NULL, 0, 0),
(67, 9, 'Une bouée', 'Gonflable', 'bc9f800d5646185b5f80f637401534c0.jpeg', 'https://www.newchic.com/fr/accessories-4999/p-1161614.html?gmcCountry=FR&currency=EUR&createTmp=1&utm_source=google&utm_medium=shopping&utm_content=yolanda&utm_campaign=pla-other-language-fr-pc&gclid=EAIaIQobChMImeiFvb3f2AIV8hXTCh2jRwzNEAQYASABEgKyMfD_BwE', '10.00', NULL, NULL, NULL, 1, 10),
(68, 5, 'Une pelle', 'Pour enterrer mon chat', 'f0ea36198d0c761b8ff5d31dc4cebcf2.jpeg', '', '10.00', NULL, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `liste`
--

CREATE TABLE IF NOT EXISTS `liste` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `expiration` date DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estPublique` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Contenu de la table `liste`
--

INSERT INTO `liste` (`no`, `user_id`, `titre`, `description`, `expiration`, `token`, `estPublique`) VALUES
(1, 1, 'Pour fêter le bac !', 'Pour un week-end à Nancy qui nous fera oublier les épreuves. ', '2018-06-27', 'nosecure1', 0),
(2, 2, 'Liste de mariage d''Alice et Bob', 'Nous souhaitons passer un week-end royal à Nancy pour notre lune de miel :)', '2018-06-30', 'nosecure2', 0),
(3, 3, 'C''est l''anniversaire de Charlie', 'Pour lui préparer une fête dont il se souviendra :)', '2017-12-12', 'nosecure3', 0),
(4, NULL, 'coucouc', NULL, NULL, NULL, 0),
(5, 2, 'La liste du big boss', 'le patroooon', '2018-08-08', 'nosecure5', 1),
(6, 3, 'zeaeza', 'zaeazzae', '2019-08-09', 'nosecure6', 0),
(7, 2, 'Bonjour bonjour', 'coucoucoucou', '2018-01-19', 'nosecure7', 0),
(8, 2, 'la liste de test', 'test', '2018-01-19', 'nosecure8', 0),
(9, 5, 'Liste d&#39;anniversaire', 'Ma liste d&#39;anniversaire', '2018-04-08', 'nosecure9', 0),
(10, 5, 'Liste de noël', 'Ma liste de cadeaux de noël', '2017-12-24', 'nosecure10', 0);

-- --------------------------------------------------------

--
-- Structure de la table `messageliste`
--

CREATE TABLE IF NOT EXISTS `messageliste` (
  `liste_id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `messageliste`
--

INSERT INTO `messageliste` (`liste_id`, `message`) VALUES
(1, 'bonjour');

-- --------------------------------------------------------

--
-- Structure de la table `participecagnotte`
--

CREATE TABLE IF NOT EXISTS `participecagnotte` (
  `IdUser` int(10) NOT NULL,
  `nomUser` varchar(1000) NOT NULL,
  `Montant` double NOT NULL,
  `idItem` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `participecagnotte`
--

INSERT INTO `participecagnotte` (`IdUser`, `nomUser`, `Montant`, `idItem`) VALUES
(4, 'Sarah', 200, 57),
(0, 'Bernard', 20, 62),
(0, 'Michel', 5, 62),
(0, 'Michel', 5, 62),
(0, 'MonNom', 0, 60),
(0, 'zeaz', 0, 60),
(0, 'defdf', 4, 60),
(0, 'sarah', 1, 60),
(0, 'jkjjk', 4, 60),
(0, 'yuyu', 3, 60),
(0, 'rtetrt', 3, 60),
(0, 'dedz', 2, 60),
(0, 'dzadza', 3, 60);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `nom` varchar(40) DEFAULT NULL,
  `prenom` varchar(40) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mdp` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `email`, `mdp`) VALUES
(1, 'Millotte', 'Baptiste', 'baptiste.email@lolol.fr', 'coucouBaptiste'),
(2, 'LeBigBoss', 'Suprème', 'bigboss@supreme.fr', '$2y$12$lVWUZKOlneyAX1eElcHpI.9RflTENTh5RC8aK46rquDgO80AE7b1O'),
(3, 'Test', 'test2', 'test@email.com', '$2y$12$nPB63MdcGNlgKCz6fX4IO.j72RhguAs/6HHcw2nrxB20MVbpPzwtS'),
(4, 'Sarah', 'Pauline', 'sarah.pauline@coco.fr', '$2y$12$8ZLqQS2Uln2lsV1iY.wPk.Bms2floRFHqGk3SVtlfb4WGTxX4szE.'),
(5, 'Prof', 'Prof', 'prof@iut.fr', '$2y$12$Gm6tcoEQwjQMWDdwoj2GE.sQDaIdWybrGTcOVjO/UnUhZMhi/RgrW');

