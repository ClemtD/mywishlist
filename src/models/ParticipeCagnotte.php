<?php

namespace mywishlist\models;

class ParticipeCagnotte extends \Illuminate\Database\Eloquent\Model{

	protected $table='participecagnotte';
	protected $primaryKey='nomUser,IdUser';
	public $timestamps=false;

	public static function insert($montant,$idItem,$nomUser,$idUser){
		$m=new ParticipeCagnotte();
		$m->IdUser=$idUser;
		$m->montant=$montant;
    $m->idItem=$idItem;
    $m->nomUser=$nomUser;
		$m->save();
	}

}
