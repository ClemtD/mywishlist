<?php

namespace mywishlist\models;

class Liste extends \Illuminate\Database\Eloquent\Model{

	protected $table='liste';
	protected $primaryKey='no';
	public $timestamps=false;

	public function items(){
		return $this->hasMany('\mywishlist\models\Item','liste_id');
	}

	public static function insert($id,$nom,$date,$description){
		$l=new Liste();
		$l->user_id=$id;
		$l->titre=$nom;
		$l->expiration=$date;
		$l->description=$description;
		$l->save();
		$l->token="nosecure".$l->no;
		$l->save();
	}

	public static function mettreAjour($t,$d,$id,$date){
		$l=Liste::find($id);
		$l->titre=$t;
		$l->description=$d;
		$l->expiration=$date;
		$l->save();
	}

	public static function rendrePublique($id){
		$l=Liste::find($id);
		$l->estPublique=1;
		$l->save();
	}

	public static function rendrePrivee($id){
		$l=Liste::find($id);
		$l->estPublique=0;
		$l->save();
	}

	public function user(){
		return $this->hasOne('mywishlist\User');
	}
}
