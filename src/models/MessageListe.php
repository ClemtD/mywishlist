<?php

namespace mywishlist\models;

class MessageListe extends \Illuminate\Database\Eloquent\Model{
			
	protected $table='messageliste';
	protected $primaryKey='no';
	public $timestamps=false;
	
	public static function insert($listid,$message){
		$m=new MessageListe();
		$m->liste_id=$listid;
		$m->message=$message;
		$m->save();
	}
	
}