<?php

namespace mywishlist\models;

class Item extends \Illuminate\Database\Eloquent\Model{

	protected $table='item';
	protected $primaryKey='id';
	public $timestamps=false;


	public function liste(){
		return $this->belongsTo('\mywishlist\models\Liste','liste_id');
	}

	public static function insert($idliste,$nom,$desc,$p,$u,$img){
		if($_FILES['img']['name']!=""){
			$nomIMG = md5(uniqid(rand(), true));
			$extension_upload=substr($_FILES['img']['type'],6,strlen($_FILES['img']['type']));
			$nomIMG=$nomIMG.".$extension_upload";
			$res=move_uploaded_file($_FILES['img']['tmp_name'],"img/".$nomIMG);
		}else {
			$nomIMG="";
		}
		$i=new Item();
		$i->liste_id=$idliste;
		$i->nom=$nom;
		$i->descr=$desc;
		$i->tarif=$p;
		$i->url=$u;
		$i->img=$nomIMG;
		$i->aCagnotte=false;
		$i->montant=0;
		$i->save();
	}

	public static function mettreAjour($id,$n,$d,$p,$u,$img){
		$i=Item::find($id);
		$ancienneIMG=$i->img;
		$nomIMG="";
		if($ancienneIMG!=""){
			unlink("img/".$ancienneIMG);
			$i->img="";
		}if($_FILES['img']['name']!=""){
			$nomIMG = md5(uniqid(rand(), true));
			$extension_upload=substr($_FILES['img']['type'],6,strlen($_FILES['img']['type']));
			$nomIMG=$nomIMG.".$extension_upload";
			$res=move_uploaded_file($_FILES['img']['tmp_name'],"img/".$nomIMG);
		}
		$i->nom=$n;
		$i->descr=$d;
		$i->tarif=$p;
		$i->url=$u;
		$i->img=$nomIMG;
		$i->save();
	}

	public static function supprimer($id){
		$i=Item::find($id);
		$i->delete();
	}

	public static function supprimerImg($id){
		$i=Item::find($id);
		$ancienneIMG=$i->img;
		if($ancienneIMG!=""){
			unlink("img/".$ancienneIMG);
		}
		$i->img="";
		$i->save();
	}

	public static function compterCagnotte($id){
		$i=Item::find($id);
		if($i->aCagnotte==1){
			$compteur=0;
			$c=array();
			$c=ParticipeCagnotte::where('idItem','=',$id)->get();
			$c=$c->toArray();
			foreach($c as $key=>$value){
				$compteur+=$value['Montant'];
			}
			return $compteur;
		}
	}
}
