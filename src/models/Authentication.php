<?php

namespace mywishlist\models;
use \mywishlist\models\User;
use \mywishlist\models\Liste;

class Authentication{

  public static function createUser($n,$p,$e,$mdp){
	 $u=User::where("email","=",$e)->get();
	if(isset($u[0])){
		return 0;
	}
	else{
		User::insert($n,$p,$e,$mdp);
		Authentication::authenticate($e,$mdp);
		return 1;
	}
  }

  public static function authenticate($email,$password){
	$u=User::where("email","=",$email)->get();
	if(isset($u[0])){
		$hash= $u[0]->mdp;
		if(password_verify($password,$hash)){
			Authentication::loadProfile($u[0]->id);
			setcookie("userid",$u[0]->id,time()+60*60*24*30);
			return 0;
		}
	}
	return 1;
  }

  public static function loadProfile($id){
	$u=User::where("id","=",$id)->get();
	$_SESSION=[];
	$_SESSION['username'] = $u[0]->nom;
	$_SESSION['userid'] = $u[0]->id;
  }

  public static function checkAccessRights($idliste){
		if(isset($_SESSION['userid'])){
			$l=Liste::where('no','=',$idliste)->get();
			$l=$l->toArray();
			if(isset($l[0])){
				$l=$l[0];
				if($_SESSION['userid']==$l['user_id']){
					return true;
				}
			}
		}
		return false;
  }

  public static function deconnexion(){
	  $_SESSION=[];
  }


}
