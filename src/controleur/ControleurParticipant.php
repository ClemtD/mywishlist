<?php

namespace mywishlist\controleur;
use \mywishlist\models\Liste as Liste;
use \mywishlist\models\Item;
use \mywishlist\models\User;
use \mywishlist\models\MessageListe as MessageListe;
use \mywishlist\models\ParticipeCagnotte as ParticipeCagnotte;

class ControleurParticipant{

	public function listerListes(){
		$list1=Liste::all();
		$vue=new \mywishlist\vue\VueParticipant($list1->toArray());
		$vue->render(1);
	}

	public function afficherListe($num){
			$l=Liste::where('token','=',$num)->first();
			$l=$l->toArray();
			$liste3=Liste::where('no','=',$l['no'])->first()->items;

			if(isset($_POST['valider_message'])&& $_POST['valider_message']=='valid_f2'){
				$message = new \mywishlist\models\MessageListe();
				$message->message = filter_var($_POST['message'],FILTER_SANITIZE_STRING);
				$message->liste_id = $l['no'];
				$message->save();
			}
			$liste3=$liste3->toArray();
			$liste3['token']=$num;
			$user=User::find($l['user_id']);
			$liste3['nom']=$user['nom'];
			$liste3['prenom']=$user['prenom'];
			$vue=new \mywishlist\vue\VueParticipant($liste3);
			$vue->render(2);
	}

	public function afficherItem($num){
		$item=Item::find($num);
		$a = $item->liste->user_id;
		$b = $item->liste->expiration;
		if(isset($_POST['valider_reservation'])&& $_POST['valider_reservation']=='valid_f1'){
			$item->nomReserveur = filter_var($_POST['name'],FILTER_SANITIZE_STRING);
			$item->message = filter_var($_POST['message'],FILTER_SANITIZE_STRING);
			if(isset($_SESSION['userid'])){
				$item->id_reserveur= $_SESSION['userid'];
			}
			$item->save();
		}
		$item->toArray();
		$item['user_id']=$a;
		$item['liste_expiration']=$b;
		$l=Liste::find($item['liste_id']);
		$item['token']=$l['token'];

		$vue=new \mywishlist\vue\VueParticipant($item);
		$vue->render(3);
	}

	public function afficheritemp(){
		if(isset($_SESSION['userid'])){
			$num = $_SESSION['userid'];
			$item=Item::where('id_reserveur','=',$num)->get();
			$vue=new \mywishlist\vue\VueParticipant([$item]);
			$vue->render(4);
		}
	}

	public function ajouterCagnotte($montant,$nom,$id){
		if(isset($_POST['valider_addCagnotte'])&& $_POST['valider_addCagnotte']=='valid_cagnotte'){
			if(isset($_SESSION['userid'])){
				$us=$_SESSION['userid'];
			}else{
				$us=0;
			}
			$nom=filter_var($nom,FILTER_SANITIZE_STRING);
			$montant=filter_var($montant,FILTER_SANITIZE_NUMBER_FLOAT);
			ParticipeCagnotte::insert($montant,$id,$nom,$us);
		}
		$this->afficherItem($id);
	}



}
