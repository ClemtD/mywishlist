<?php

namespace mywishlist\controleur;
use \mywishlist\models\Liste as Liste;
use \mywishlist\models\Item;
use \mywishlist\models\User;
use \mywishlist\models\Authentication;
use \mywishlist\models\ParticipeCagnotte;

class ControleurCreateur{

	public function creerAffListe(){
		$vue = new \mywishlist\vue\VueCreateur([]);
		$vue->render(1);
	}

	//implementer quand un utilisateua pas co
	public function creerListe($nom,$date,$descr){
		if (isset($_SESSION['userid']) && isset($_POST['valider_liste'])&& $_POST['valider_liste']=='valid_f1'){
		$n=filter_var($nom,FILTER_SANITIZE_STRING);
		$d=filter_var($descr,FILTER_SANITIZE_STRING);
		$da=strtotime($date);
			if($da>time()){
				Liste::insert($_SESSION['userid'],$n,$date,$d);

				$liste=Liste::where('user_id','=',$_SESSION['userid'])->get();
				$vue = new \mywishlist\vue\VueCreateur($liste);
				$vue->render(2);
			}else{
				$vue = new \mywishlist\vue\VueCreateur([]);
				$vue->render(20);
			}
		}else{
		$vue = new \mywishlist\vue\VueCreateur([]);
		$vue->render(22);
		}
	}

	public function creerAffInscription(){
		$vue = new \mywishlist\vue\VueCreateur([]);
		$vue->render(3);
	}

	public function creerUser($n,$p,$e,$mdp){
		if (isset($_POST['valider_inc'])&& $_POST['valider_inc']=='valid_f2'){
			$n=filter_var($n,FILTER_SANITIZE_STRING);
			$p=filter_var($p,FILTER_SANITIZE_STRING);

			$mdp=filter_var($mdp,FILTER_SANITIZE_STRING);
			$bool=0;
			if(filter_var($e,FILTER_VALIDATE_EMAIL)){
				$bool=Authentication::createUser($n,$p,$e,$mdp);
			}
			if($bool==1){
			$vue = new \mywishlist\vue\VueCreateur([]);
			$vue->render(4);
			}else{
			$vue = new \mywishlist\vue\VueCreateur([]);
			$vue->render(3);
		}

		}
	}

	public function connectionUser($email,$mdp){
		if (isset($_POST['valider_conn']) && $_POST['valider_conn']=='valid_f3'){
			$bool=Authentication::authenticate($email,$mdp);
			if($bool==0){
				$vue = new \mywishlist\vue\VueCreateur([]);
				$vue->render(7);
			}else{
				$vue = new \mywishlist\vue\VueCreateur([]);
				$vue->render(8);
			}
		}else{
			$vue = new \mywishlist\vue\VueCreateur([]);
			$vue->render(8);
		}
	}

	public function afficherConnexion(){
	$vue =new \mywishlist\vue\VueCreateur([]);
	$vue->render(6);
	}

	public function afficherListes(){
		if(isset($_SESSION['userid'])){
			$l=Liste::where('user_id','=',$_SESSION['userid'])->get();
			$vue =new \mywishlist\vue\VueCreateur($l);
			$vue->render(2);
		}
		else{
			$vue = new \mywishlist\vue\VueCreateur([]);
			$vue->render(22);
		}
	}

	public function afficherListe($num){
		if(isset($_SESSION['userid']) && Authentication::checkAccessRights($num)){
			$l=Liste::where('no','=',$num)->first()->items;
			$l2=Liste::where('no','=',$num)->first();
			$l2=$l2->toArray();
			$l=$l->toArray();
			$l['idliste']=$num;
			$l['estPublique']=$l2['estPublique'];
			$l['date']=$l2['expiration'];
			$vue =new \mywishlist\vue\VueCreateur($l);
			$vue->render(11);
		}
		else{
			$vue = new \mywishlist\vue\VueCreateur([]);
			$vue->render(22);
		}
	}

	public function afficherAccueil(){
		$vue=new \mywishlist\vue\VueCreateur([]);
		$vue->render(4);
	}

	public function affajouterItem(){
		$vue = new \mywishlist\vue\VueCreateur([]);
		$vue->render(5);
	}

	public function afficherDeconnexion(){
		Authentication::deconnexion();
		$vue = new \mywishlist\vue\VueCreateur([]);
		$vue->render(9);
	}

	public function creerAffModifCreateur(){
		if(isset($_SESSION['userid'])){
			$u=User::where('id','=',$_SESSION['userid'])->get();
			$u=$u->toArray();
			$u=$u[0];

			$vue = new \mywishlist\vue\VueCreateur($u);
			$vue->render(10);
		}
		else{
			$vue = new \mywishlist\vue\VueCreateur([]);
			$vue->render(22);
		}
	}

	public function modifCreateur($n,$p,$amdp,$nmdp){
		if (isset($_POST['valider_modifUser'])&& $_POST['valider_modifUser']=='valid_f5'){
			$n=filter_var($n,FILTER_SANITIZE_STRING);
			$p=filter_var($p,FILTER_SANITIZE_STRING);
			User::mettreAjour($n,$p,$amdp,$nmdp);
			$this->creerAffModifCreateur();
		}
	}

	public function afficherItem($num){
		if(isset($_SESSION['userid'])){
			$item=Item::find($num);
			$item=$item->toArray();
			$item['iditem']=$num;
			$vue = new \mywishlist\vue\VueCreateur($item);
			$vue->render(12);
		}else{
			$vue = new \mywishlist\vue\VueCreateur([]);
			$vue->render(22);
		}
	}

	public function creerAffModifListe($num){
		if(Authentication::checkAccessRights($num)==true){
			$l=Liste::where('no','=',$num)->first();
			$l=$l->toArray();
			$l['idliste']=$num;
			$vue = new \mywishlist\vue\VueCreateur($l);
			$vue->render(13);
		}
	}

	public function modifListe($t,$d,$id,$date){
		if(Authentication::checkAccessRights($id)==true && isset($_POST['valider_modifListe'])&& $_POST['valider_modifListe']=='valid_f6'){
			$t=filter_var($t,FILTER_SANITIZE_STRING);
			$d=filter_var($d,FILTER_SANITIZE_STRING);
			if(strtotime($date)>time()){
			Liste::mettreAjour($t,$d,$id,$date);
			}
			$this->creerAffModifListe($id);
		}
	}

	public function creerAffAjouterItem($num){
		if(Authentication::checkAccessRights($num)==true){
			$l['idliste']=$num;
			$vue = new \mywishlist\vue\VueCreateur($l);
			$vue->render(5);
		}
	}


	public function ajouterItem($num,$nom,$descr,$p,$u,$img){
		if(Authentication::checkAccessRights($num)==true && isset($_POST['valider_item'])&& $_POST['valider_item']=='valid_f4'){
			$n=filter_var($nom,FILTER_SANITIZE_STRING);
			$d=filter_var($descr,FILTER_SANITIZE_STRING);
			$u=filter_var($u,FILTER_SANITIZE_URL);
			Item::insert($num,$n,$d,$p,$u,$img);
			$this->afficherListe($num);

			$_FILES['img']['tmp_name']="";
		}

	}

	public function afficherModifItem($num){
		$i=Item::find($num);
		$i=$i->toArray();
		$i['id']=$num;
		if($i['nomReserveur']==null){
			$vue = new \mywishlist\vue\VueCreateur($i);
			$vue->render(14);
		}else{
			$this->afficherItem($num);
		}
	}

	public function modifierItem($num,$nom,$descr,$prix,$url,$img){
		$n=filter_var($nom,FILTER_SANITIZE_STRING);
		$d=filter_var($descr,FILTER_SANITIZE_STRING);
		$u=filter_var($url,FILTER_SANITIZE_URL);
		Item::mettreAjour($num,$n,$d,$prix,$u,$img);
		$this->afficherItem($num);
	}

	public function supprimerItem($num){
			$id=Item::find($num);
			$id=$id->toArray();
			$id=$id['liste_id'];
			if($id['nomReserveur']==null){
			Item::supprimer($num);
			$this->afficherListe($id);
		}
	}

	public function supprimerImg($num){
		Item::supprimerImg($num);
		$this->afficherItem($num);
	}


	public function rendrePublique($num){
		if(Authentication::checkAccessRights($num)==true && isset($_POST['rendre_publique'])&& $_POST['rendre_publique']=='valid_Publique'){
			Liste::rendrePublique($num);
		}
		$this->afficherListe($num);
	}

	public function rendrePrivee($num){
		if(Authentication::checkAccessRights($num)==true && isset($_POST['rendre_privee'])&& $_POST['rendre_privee']=='valid_Privee'){
			Liste::rendrePrivee($num);
		}
		$this->afficherListe($num);
	}

	public function afficherListesPubliques(){
		$liste=Liste::where('estPublique','=',1);
		$liste=$liste->orderBy('expiration')->get();
		$vue = new \mywishlist\vue\VueCreateur($liste);
		$vue->render(15);
	}

	public function afficherListePublique($num){
		$liste=Liste::where('no','=',$num)->first()->items;
		$vue = new \mywishlist\vue\VueCreateur($liste);
		$vue->render(16);
	}

	public function partagerListe($id){
		$l;
		if(Authentication::checkAccessRights($id)){
			$l=Liste::where('no','=',$id)->first()->items;
			$l2=Liste::where('no','=',$id)->first();
			$l2=$l2->toArray();
			$l=$l->toArray();
			$l['idliste']=$id;
			$l['estPublique']=$l2['estPublique'];
			$l['date']=$l2['expiration'];


		 $url='http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		 $url=$this->before_last('/',$url);
		 $url=$this->before_last('/',$url);
		 $url=$url."/afficherliste/";
		 $l3=Liste::where('no','=',$id)->first();
		 $l3=$l3->toArray();
		 $tok=$l3['token'];
		 $url=$url.$tok;
		 $l['url']=$url;
		 $vue = new \mywishlist\vue\VueCreateur($l);
		$vue->render(17);
		}
	}

	function before_last ($t, $inthat)
    {
        return substr($inthat, 0, $this->strrevpos($inthat, $t));
    }

	function strrevpos($instr, $needle)
{
    $rev_pos = strpos (strrev($instr), strrev($needle));
    if ($rev_pos===false) return false;
    else return strlen($instr) - $rev_pos - strlen($needle);
}

public function consulterUtilisateurs(){
	if(isset($_SESSION['userid'])){
		$liste=Liste::where('estPublique','=',1)->get();
		$liste=$liste->toArray();
		$listeUsers=array();
		foreach($liste as $key=>$value){
			$u=User::where('id',"=",$value['user_id'])->first();
			if(!array_key_exists($value['user_id'],$listeUsers)){
				$listeUsers[$value['user_id']]=$u;
			}
		}
		$vue = new \mywishlist\vue\VueCreateur($listeUsers);
		$vue->render(18);
	}
}

	public function confirmer(){
		if(isset($_SESSION['userid']) && isset($_POST['del_compte'])&& $_POST['del_compte']=='valid_delete'){
			$vue = new \mywishlist\vue\VueCreateur([]);
			$vue->render(19);
		}else{
			$vue = new \mywishlist\vue\VueCreateur([]);
			$vue->render(4);
		}
	}

	public function supprimerCompte(){
		if(isset($_SESSION['userid']) && isset($_POST['del_compte2'])&& $_POST['del_compte2']=='valid_delete2'){
			$l=Liste::where('user_id','=',$_SESSION['userid'])->get();
			$l=$l->toArray();
			foreach($l as $key=>$value){
				$litems=Item::where('liste_id','=',$value['no'])->get();
				$litems=$litems->toArray();
				foreach($litems as $key2=>$value2){
					if($value2['img']=""){
						unlink("img/".$value2['img']);
					}
					$i=Item::where('id','=',$value2['id'])->first();
					$i->delete();
				}
				$listeDel=Liste::where('no','=',$value['no'])->first();
				$listeDel->delete();
			}
			$itemsRes=Item::where('id_reserveur','=',$_SESSION['userid'])->get();
			$itemsRes=$itemsRes->toArray();
			foreach($itemsRes as $key=>$value){
				$i=Item::find($value['id']);
				$i->id_reserveur="";
				$i->message="";
				$i->nomReserveur="";
				$i->save();
			}



		}

		Authentication::deconnexion();
		$this->afficherAccueil();
	}

	public function creerCagnotte($id){
		$idl;
		if(isset($_SESSION['userid']) && isset($_POST['valider_cagnotte'])&& $_POST['valider_cagnotte']=='valid_cagnotte'){
			$item=Item::find($id);
			$idl=$item['liste_id'];
			$item->aCagnotte=True;
			$item->montant=$item['tarif'];
			$item->save();
		}
		$this->afficherListe($idl);
	}

	public function pageContact(){
		$vue=new \mywishlist\vue\VueCreateur([]);
		$vue->render(21);
	}




}
