<?php

namespace mywishlist\vue;
use \mywishlist\models\MessageListe as MessageListe;
use \mywishlist\models\Item;
use \mywishlist\models\ParticipeCagnotte;

class VueCreateur{

	private $tableau;

	public function __construct($tab){
		$this->tableau=$tab;
	}

	public function afficherCreationListe(){
		$code=<<<END
<form id="crea" method= "post" action = "creerliste">
<label for="f1_nom"> Nom :</label>
<input type = "text" id="f1_nom" name="nom" placeholder="<nom de la liste>" required>
<label for="f1_desc"> Description :</label>
<input type = "text" id="f1_desc" name="desc" placeholder="<description de la liste>" >
<label for="f1_dateexp">Date d'expiration :</label>
<input type = "date" id="f1_dateexp" name="date" placeholder="<Date d'expiration>" required>
<button type="submit" name="valider_liste"  value="valid_f1">Valider</button>
</form>
END;
	return $code;
	}

	public function afficherCreationItem(){
		$id=$this->tableau['idliste'];
		$code=<<<END
<form id="crea" enctype="multipart/form-data" method="post" action ="../valideritem/$id">
<label for="f4_nom"> Nom :</label>
<input type = "text" id="f4_nom" name="nom" placeholder="<nom item>" required>
<label for="f4_desc"> Description :</label>
<input type = "text" id="f4_desc" name="descr" placeholder="<description>" required>
<label for="f4_prix"> Prix :</label>
<input type = "number" id="f4_prix" name="prix" step="0.01" placeholder="<prix>" required>
<label for="f4_url"> URL :</label>
<input type = "text" id="f4_url" name="url" placeholder="<url>">
<label for="f4_img"> Image :</label>
<input type="hidden" name="MAX_FILE_SIZE" value="3000000" />
<input type = "file" id="f4_img" name="img" placeholder="<image>" accept="image/*">
<button type="submit" name="valider_item"  value="valid_f4">Valider</button>
</form>
END;

$button=<<<END
<form id="retour" method="get" action ="../afficherlisteCrea/$id">
<button type="submit" name="valider_retour" value="valid_retour">Retour à la liste</button>
</form>
END;
		$code=$code.$button;

	return $code;
	}


	public function afficherListes(){
		$code= "<section><ul>";
		foreach($this->tableau as $key=>$value){
			$code=$code." <li><a href='afficherlisteCrea/".$value['no']."'>".$value['titre']."</a> </li><br>";
		}
		$code=$code."</ul></section>";
		return $code;
	}

	private function afficherListe(){
		$code= "<section><ul>";
		$id=$this->tableau['idliste'];
		$date=$this->tableau['date'];
		$date=strtotime($date);
		if($date>time()){
			$buttonModif=<<<END
<form id="modifliste" method="get" action ="../affichermodifliste/$id">
<button type="submit" name="valider_modifliste" value="valid_modifliste">Modifier la liste</button>
</form>
END;

$buttonAjout=<<<END
<form id="ajoutitem" method="get" action ="../affajouteritem/$id">
<button type="submit" name="valider_ajoutitem" value="valid_ajoutitem">Ajouter un item</button>
</form>
END;
$buttonPartage=<<<END
<form id="partager" method="get" action ="../partagerliste/$id">
<button type="submit" name="valider_partage" value="valid_partage">Partager la liste</button>
</form>
END;
		$code=$code.$buttonModif.$buttonAjout.$buttonPartage."<br><br>";
		}else{
			$code="";
		}
		if (isset($this->tableau[0])){
		$this->tableau['dernier']=$this->tableau[0];
		}
		foreach($this->tableau as $key=>$value){
			if($key!='idliste'&& $key!='estPublique' && $key!='date' && $key!='url'){
				if($value['img']!=""){
					$code=$code.'<img src="../../img/'.$value['img'].'" width = "150" height="150"></img>';
				}
				if($date>time()){
					$code=$code." <li><a href='../afficheritemcrea/".$value['id']."'>".$value['nom']."</a></li>";
					if($value['aCagnotte']==0){
						if($value['nomReserveur'] != NULL){
							$code = $code."est reservé<br><br>";
						}else{
							$code = $code."n'est pas reservé<br><br>";
						}
					}else{
						$code = $code."Vous avez ouvert une cagnotte<br><br>";
					}
				}else{
					$code=$code.$value['nom']."<br>";
					if($value['aCagnotte']==0){
						if($value['nomReserveur'] != NULL){
							$code = $code."a été reservé par ".$value['nomReserveur']." : ".$value['message']."<br><br>";
						}else{
							$code = $code."n'a pas été reservé<br><br>";
						}
					}else{
						$tot=Item::compterCagnotte($value['id']);
						$code = $code." Votre cagnotte a reçu ".$tot."€ sur un total de ".$value['tarif']."<br><br>";
						$participants=ParticipeCagnotte::where('idItem','=',$value['id'])->get();
						foreach($participants as $key2=>$value2){
							$code = $code.$value2['nomUser'].' a participé à hauteur de '.$value2['Montant']."€ <br><br>";
						}



					}
				}
			}
		}
		if($date<time()){
			$messages = MessageListe::where('liste_id','=',$id)->get();
			if(isset($messages[0])){
				$code=$code."Messages : <br><br>";
				foreach($messages as $message){
				$code = $code.$message->message."<br><br>";
			}
		}
		}
		$code=$code."</ul></section><br>";

		if($this->tableau['estPublique']==0){
			$code=$code.<<<END
			<form id="crea" method="post" action ="../rendrepublique/$id">
			<button type="submit" name="rendre_publique"  value="valid_Publique">Rendre publique</button>
			</form>
END;
		}else{
			$code=$code.<<<END
			<form id="crea" method="post" action ="../rendreprivee/$id">
			<button type="submit" name="rendre_privee"  value="valid_Privee">Rendre privée</button>
			</form>
END;
		}
		return $code;
	}

	public function afficherInscription(){
$code=<<<END
<form id="crea" method= "post" action = "validerInscription">
<label for="f2_nom"> Nom :</label>
<input type = "text" id="f2_nom" name="nom" placeholder="<Nom>" required>
<label for="f2_prenom"> Prénom :</label>
<input type = "text" id="f2_prenom" name="prenom" placeholder="<Prenom>" required>
<label for="f2_email"> Email :</label>
<input type = "email" id="f2_email" name="email" placeholder="<Email>" required>
<label for="f2_mdp"> Mot de passe :</label>
<input type = "password" id="f2_mdp" name="mdp" placeholder="<Mot de passe>" required>


<button type="submit" name="valider_inc"  value="valid_f2">Valider</button>
</form>
END;
	return $code;
	}

	public function connexion(){
$code=<<<END
<form id="crea" method= "post" action = "verifierconnexion">
<label for="f3_email"> Email :</label>
<input type = "text" id="f3_email" name="email" placeholder="<Email>" required>
<label for="f3_mdp"> Mot de passe :</label>
<input type = "password" id="f3_mdp" name="mdp" placeholder="<Mot de passe>" required>
<button type="submit" name="valider_conn"  value="valid_f3">Valider</button>
</form>
END;
	return $code;
	}

	public function connexionEchec(){
		$code=$this->connexion();
$c=<<<END
<p>La connexion a échouée</php>
END;
return $code.$c;
	}

	public function connexionReussie(){
$code=<<<END
<p>Connecté</php>
END;
return $code;
	}

	public function deconnexion(){
$code=<<<END
<p>Déconnecté</php>
END;
return $code;
	}

	public function accueil(){
$code=<<<END
<p>Bienvenue sur MyWishList !!<br>MyWishList est LE site de référérence pour créer des listes de cadeaux !<br>Votre anniversaire,
noël ou une autre occasion ? Plus besoin de dire à chacun de vos invités quoi acheter et créez simplement votre liste de souhaits !</p>
 <img src="../img/cadeauxAcc.jpg" alt="cadeaux">
END;
return $code;
	}

	public function affModifCreateur(){
		$name=$this->tableau['nom'];
		$prenom=$this->tableau['prenom'];
		$code=<<<END
<form id="crea" method= "post" action = "validermodifuser">
<label for="f5_nom"> Nom :</label>
<input type = "text" id="f5_nom" name="nom" value="$name" placeholder="<Nom>" required>
<label for="f5_prenom"> Prenom :</label>
<input type = "text" id="f5_prenom" name="prenom" value="$prenom" placeholder="<Prenom>" required>
<label for="f5_amdp"> Mot de passe :</label>
<input type = "password" id="f5_amdp" name="amdp" placeholder="<Mot de passe>" required>
<label for="f5_nmdp"> Nouveau mot de passe :</label>
<input type = "password" id="f5_nmdp" name="nmdp" placeholder="<Nouveau mot de passe>">
<button type="submit" name="valider_modifUser"  value="valid_f5">Valider</button>
</form><br>
<form id="crea" method= "post" action = "demanderconfirmation">
<button type="deleteAccount" name="del_compte"  value="valid_delete">Supprimer votre compte</button>
</form><br>
END;
	return $code;
	}

	public function afficherModifListe(){
		$titre=$this->tableau['titre'];
		$desc=$this->tableau['description'];
		$id=$this->tableau['idliste'];
		$date=$this->tableau['expiration'];
		$datemin=time();
		$datemin=date('Y-m-d',$datemin);
	$code=<<<END
<form id="crea" method= "post" action = "../validermodifliste/$id">
<label for="f6_titre"> Titre :</label>
<input type = "text" id="f6_titre" name="titre" value="$titre" placeholder="<Titre>" required>
<label for="f6_desc"> Description :</label>
<input type = "text" id="f6_desc" name="desc" value="$desc" placeholder="<Description>" required>
<label for="f6_date"> Date de fin :</label>
<input type = "date" id="f6_date" name="date" value="$date" min=$datemin required>

<button type="submit" name="valider_modifListe" value="valid_f6">Valider</button>
</form>
<br>
<form id="retour" method="get" action ="../afficherlisteCrea/$id">
<button type="submit" name="valider_retour" value="valid_retour">Retour à la liste</button>
</form>
END;
	return $code;
	}

	public function afficherItem(){
		$id=$this->tableau['iditem'];
		$prix=$this->tableau['tarif'];
		$idliste=$this->tableau['liste_id'];
		$code= "<section>";
		if($this->tableau['aCagnotte']==0 && $this->tableau['nomReserveur']==NULL){
		$buttonModif=<<<END
<form id="modifitem" method="get" action ="../affichermodifitem/$id">
<button type="submit" name="valider_modifitem" value="valid_modifitem">Modifier l'item </button>
</form>
END;
$buttonSuppr=<<<END
<form id="suppritem" method="get" action ="../supprimeritem/$id">
<button type="submit" name="valider_suppritem" value="valid_suppritem">Supprimer l'item</button>
</form>
END;
		$code=$code.$buttonModif.$buttonSuppr."<br><br>";
		}

		$code=$code ."Nom item : ". $this->tableau['nom'] . "<br/>";
		$code=$code . "Description de l'item : ".$this->tableau['descr'] . "<br/>";
		if($this->tableau['url']!=""){
			$code=$code ."URL : ".$this->tableau['url']. "<br/>";
		}
		if($this->tableau['img']!=""){
			$code=$code.'<img src="../../img/'.$this->tableau['img'].'" width = "150" height="150"></img>';
		}

		if($this->tableau['nomReserveur']=="" && $this->tableau['aCagnotte']==0){
			$code=$code." N'est pas réservé.<br>";
		}else{
			if($this->tableau['nomReserveur']!=""){
				$code=$code." Est réservé.<br>";
			}

		}
		$c="";
		if($this->tableau['nomReserveur']==""){

			if($this->tableau['aCagnotte']==0){
			$c=<<<END
			<form id="cagnotte" method= "post" action = "../creerCagnotte/$id">
			<button type="submit" name="valider_cagnotte" value="valid_cagnotte">Créer une cagnotte</button>
			</form><br/>
END;
			}else{
				$m=$this->tableau['montant'];
				$c=<<<END
				<p>Vous avez ouvert une cagnotte de $m €</p>
END;
			}
		}
		$code=$code.$c;
		$button=<<<END
<form id="retour" method="get" action ="../afficherlisteCrea/$idliste">
<button type="submit" name="valider_retour" value="valid_retour">Retour à la liste</button>
</form>
END;
		$code=$code.$button;
		return $code;
	}

	public function afficherModifItem(){
		$nom=$this->tableau['nom'];
		$d=$this->tableau['descr'];
		$u=$this->tableau['url'];
		$p=$this->tableau['tarif'];
		$id=$this->tableau['id'];
		$img=$this->tableau['img'];
		$montant=$this->tableau['montant'];
		$idliste=$this->tableau['liste_id'];
		$code=<<<END
<form id="crea" enctype="multipart/form-data" method= "post" action = "../validermodifitem/$id">
<label for="f7_nom"> Nom :</label>
<input type = "text" id="f7_nom" name="nom" value="$nom" placeholder="<Nom>" required>
<label for="f7_desc"> Description :</label>
<input type = "text" id="f7_desc" name="desc" value="$d" placeholder="<Description>" required>
<label for="f7_prix"> Prix :</label>
<input type = "number" id="f7_prix" name="prix" value="$p" step="0.01" min=0 placeholder="<Prix>" required>
<label for="f7_url"> URL :</label>
<input type = "text" id="f7_url" name="url" value="$u" placeholder="<URL>">
<input type="hidden" name="MAX_FILE_SIZE" value="3000000" />
<input type = "file" id="f7_img" name="img" placeholder="<image>" accept="image/*">
<button type="submit" name="valider_modifitem" value="valid_f7">Valider</button>
</form><br/>


END;
$c="";
if($img!=""){
$c=<<<END
<form id="crea" method= "post" action = "../supprimerimg/$id">
<button type="delete" name="delImg" value="del_f7">Supprimer l'image</button>
END;
}
$code=$code.$c;
$code=$code."</form>";

	$button=<<<END
<form id="retour" method="get" action ="../afficherlisteCrea/$idliste">
<button type="submit" name="valider_retour" value="valid_retour">Retour à la liste</button>
</form>
END;
		$code=$code.$button;

	return $code;

	}

	public function afficherListesPubliques(){
		if(isset($_SESSION['userid'])){
			$code= "<section><ul>";
			foreach($this->tableau as $key=>$value){
				$code=$code." <li><a href='afficherlistepublique/".$value['no']."'>".$value['titre']."</a> </li><br>";
			}
			$code=$code."</ul></section>";
		}else{
			$code= "<section><ul>";
			foreach($this->tableau as $key=>$value){
				$code=$code." <li><p>".$value['titre']."</p></li><br>";
			}
			$code=$code."</ul></section>";
		}
		return $code;
	}

	public function afficherListePublique(){
		$code= "<section><ul>";
		$idliste = $this->tableau[0]['liste_id'];
		foreach($this->tableau as $key=>$value){
			$code=$code.$value['nom']."<br>";
			if($value['img']!=""){
			$code=$code.'<img src="../../img/'.$value['img'].'" width = "150" height="150"></img>';
			}
		}
		$code=$code."</ul></section>";
				$button=<<<END
<form id="retour" method="get" action ="../afficherlistespubliques">
<button type="submit" name="valider_retour" value="valid_retour">Aux listes publiques</button>
</form>
END;
		$code=$code.$button;
		return $code;
	}

	public function afficherPartage(){
		$code="";
		$url=$this->tableau['url'];
		$id=$this->tableau['idliste'];
		$code="URL à envoyer à vos contacts : "."<input value=$url type=\"text\" id=\"ftruc_url\" name=\"url\" >"."<br><br>";
		//$code=$code.$this->afficherListe();
		$button=<<<END
<form id="retour" method="get" action ="../afficherlisteCrea/$id">
<button type="submit" name="valider_retour" value="valid_retour">Retour à la liste</button>
</form>
END;
		$code=$code.$button;
		return $code;
	}

	public function afficherUtilisateurs(){
		$code="<section><ul>";
		foreach($this->tableau as $key=>$value){
			$code=$code.$value['email']."<br>";
		}
		$code=$code."</ul></section>";
		return $code;
	}

	public function demanderConfirm(){
		$code=<<<END
<p>Etes-vous sur de vouloir supprimer votre compte? Toute suppression est immédiate et irreversible.
Toutes vos listes seront supprimées et ne pourront pas être récupérées</p>
<form id="crea" method= "post" action = "supprimerCompte">
<button type="deleteAccount" name="del_compte2"  value="valid_delete2">Supprimer votre compte</button>
</form><br>
END;
	return $code;
	}

	public function dateInvalide(){
		$code="<p>Désolé la date est déjà passée.</p>";
		$code=$code.$this->afficherCreationListe();
		return $code;
	}

	public function contact(){
		$code=<<<END
<section><ul><p>Site réalisé par :<br>Clément Domps<br>Sarah Combe Deschaumes<br>
Baptiste Millotte<br>
Jimmy Noël<br>
</p></ul></section>


END;
	return $code;
	}

	public function nonConnecte(){
		$code=<<<END
		<p>Vous n'êtes pas connecté</p>
END;
		return $code;
	}

	public function afficherBoutons($identation){
		if(isset($_SESSION['userid'])){
			$res="<a class=\"co\" href=\"$identation
			deconnexion\">Déconnection</a>";
		}else{
			$res="<a class=\"co\" href=\"$identation
			inscription\">Inscription</a>
<a class=\"co\" href=\"$identation
connexion\">Connection</a>";
		}
		return $res;
	}

	public function genererNav($identation){
		$res="";
		if(isset($_SESSION['userid'])){
			$res="<li><a href=\"$identation
			afficherlistesCreateur\">Mes Listes</a></li><li><a href=\"$identation
			affichermodifcreateur\">Compte</a><a href=\"$identation
			utilisateurs\">Liste des utilisateurs</a></li><li><a href=\"$identation
affichercrealiste\">Créer une liste</a></li>";
		}
		$res=$res."<li><a href=\"$identation
			afficherlistespubliques\">Listes publiques</a></li>";
		return $res;
	}

	public function render($int){
		$identation="";
		switch($int){
			case 1: {
				$content=$this->afficherCreationListe();
				break;
			}
			case 2: {
				$content=$this->afficherListes();
				break;
			}
			case 3: {
				$content=$this->afficherInscription();
				break;
			}
			case 4: {
				$content=$this->accueil();
				break;
			}
			case 5: {
				$identation="../";
				$content=$this->afficherCreationItem();
				break;
			}
			case 6: {
				$content=$this->connexion();
				break;
			}
			case 7: {
				$content=$this->connexionReussie();
				break;
			}
			case 8: {
				$content=$this->connexionEchec();
				break;
			}
			case 9: {
				$content=$this->deconnexion();
				break;
			}
			case 10: {
				$content=$this->affModifCreateur();
				break;
			}
			case 11: {
				$identation="../";
				$content=$this->afficherListe();
				break;
			}
			case 12:{
				$identation="../";
				$content=$this->afficherItem();
				break;
			}
			case 13:{
				$identation="../";
				$content=$this->afficherModifListe();
				break;
			}
			case 14:{
				$identation="../";
				$content=$this->afficherModifItem();
				break;
			}
			case 15:{
				$content=$this->afficherListesPubliques();
				break;
			}
			case 16:{
				$identation="../";
				$content=$this->afficherListePublique();
				break;
			}
			case 17:{
				$identation="../";
				$content=$this->afficherPartage();
				break;
			}
			case 18:{
				$content=$this->afficherUtilisateurs();
				break;
			}
			case 19:{
				$content=$this->demanderConfirm();
				break;
			}
			case 20:{
				$content=$this->dateInvalide();
				break;
			}
			case 21:{
				$content=$this->contact();
				break;
			}
			case 22:{
				$content=$this->nonConnecte();
				break;
			}


		}
		$res=$this->afficherBoutons($identation);
		$nav=$this->genererNav($identation);
		$code= <<<END
<!DOCTYPE html>
<html lang="fr">
<head>
<title>mywishlist</title>
<link rel="shortcut icon" href="../$identation/img/favicon.ico">
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="stylesheet" href="../$identation
test.css" />
</head>
<body>
<div id="userpanel">
<ul>
$res
</div>
<section class="head">

<h2>My WishList</h2>
</section>
<!-- Barre de navigation-->
<div class="redline"></div>
<nav>
<ul class="nav1">   <li><a href="$identation
home">Accueil</a></li>
$nav
</ul>
<ul class="nav2">

</ul>
</nav>
<div class="redline"></div>
$content
<footer>
<ul class="footer"> <li class="footer"><a href="$identation
contact">Contact</a></li></ul>
</footer>
</body>
</html>
END;
	echo $code;
	}
}
