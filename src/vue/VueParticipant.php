<?php

namespace mywishlist\vue;
use \mywishlist\models\MessageListe as MessageListe;
use \mywishlist\models\Item as Item;
use \mywishlist\models\Liste;
use \mywishlist\models\User;

class VueParticipant{

	private $tableau;

	public function __construct($tab){
		$this->tableau=$tab;
	}

	private function afficherListes(){
		$code= "<section><ul>";
		foreach($this->tableau as $key=>$value){
			$code=$code." <li><a href='afficherliste/".$value['token']."'>".$value['titre']."</a> </li><br>";
		}
		$code=$code."</ul></section>";
		return $code;
	}

	private function afficherListe(){
		$idliste = $this->tableau[0]['liste_id'];
		$liste = Liste::where('no','=',$idliste)->first();
		$code = '<br><p> Liste ' . $liste->titre . ' de '.$this->tableau['nom'].' '.$this->tableau['prenom'].' : ' . $liste->description . '</p>';
		$code = $code . '<br><p> Cette liste expire le ' . $liste->expiration . '.</p><br> Voici les items présents dans cette liste : ';
		$code= $code . "<section><ul>";
		$token = $this->tableau['token'];
		if(isset($this->tableau[0]['liste_id'])){
			$this->tableau['dernier']=$this->tableau[0];
		}
		$idcreateur = Liste::where('no','=',$idliste)->first()->user_id;
		foreach($this->tableau as $key=>$value){

			if($key!="token" && $key!="nom" && $key !="prenom"){
			$code=$code." <li><a href='afficheritem/".$value['id']."'>".$value['nom']."</a></li><br>";

			if($value['img']!=""){
					$code=$code.'<img src="../../img/'.$value['img'].'" width = "150" height="150"></img><br>';
			}


			if($value['aCagnotte']==0){

					if($value['nomReserveur'] != NULL){
						$code = $code."A été réservé<br><br>";
					}else{
						$code = $code."N'est pas réservé<br><br>";
					}
			}else{
				$montant=Item::compterCagnotte($value['id']);
				if($montant<$value['tarif']){
				$code = $code."une cagnotte est ouverte pour cet item.<br><br>";
				}else{
					$code=$code."La cagnotte est pleine.<br><br>";
				}
			}

		}
	}

	if((isset($_COOKIE["userid"]) && $_COOKIE["userid"]!=$idcreateur )|| !isset($_COOKIE['userid'])){
	$code= $code.<<<END
	<form id = "f1" method="post" action = "./$token">
	<label for "f1_message">Envoyer un message publique:</label>
	<input type="text" id="f1_message" name="message" >
	<button type="submit" name="valider_message" value="valid_f2">Envoyer</button>
	</form>
END;



		$messages = MessageListe::where('liste_id','=',$idliste)->get();
		if (isset($messages[0])){
			$code = $code."<br><br>Messages :<br><br>";
			foreach($messages as $message){
				$code = $code.$message->message."<br><br>";
			}
		}
	}
		$code=$code."</ul></section>";
		return $code;

	}

	private function afficherBoutons($identation){
		if(isset($_SESSION['userid'])){
			$res="<a class=\"co\" href=\"$identation
			deconnexion\">Déconnection</a>";
		}else{
			$res="<a class=\"co\" href=\"$identation
			inscription\">Inscription</a>
<a class=\"co\" href=\"$identation
connexion\">Connection</a>";
		}
		return $res;
	}

	private function afficherItem(){
		$code= "<section><br>";
		$code=$code . $this->tableau['nom'] . " : " . $this->tableau['tarif'] ."€<br/><br>";
		$code=$code . ' Description : ' . $this->tableau['descr'] . "<br/>";
		if($this->tableau['url']!=""){
			$code=$code ."URL : ".$this->tableau['url']. "<br/><br>";
		}
		$idliste = $this->tableau['liste_id'];
		$idcreateur = Liste::where('no','=',$idliste)->first()->user_id;
		//$code=$code.'<img src="../../../img/'.$this->tableau['img'].'" width = "150" height="150"></img>';
		$user="";
		if (isset($_SESSION['userid'])){
			$user = $_SESSION['username'];
		}

		if($this->tableau['img']!=""){
			$code=$code.'<img src="../../../img/'.$this->tableau['img'].'" width = "150" height="150"></img><br>';
		}

		if(strtotime($this->tableau['liste_expiration'])>time()){
			if($this->tableau['aCagnotte']==0){
				if($this->tableau['nomReserveur'] == NULL){
					if((isset($_COOKIE["userid"]) && $_COOKIE["userid"]!=$idcreateur )|| !isset($_COOKIE['userid'])){

					$itemid = $this->tableau['id'];
		$code= $code.<<<END
			<form id = "f1" method="post" action = "../../afficheritem/reserver/$itemid">
			<label for="f1_name">Nom:</label>
			<input type="text" id="f1_name" name="name" value="$user" required>
			<label for "f1_message">Message:</label>
			<input type="text" id="f1_message" name="message" >
			<button type="submit" name="valider_reservation"value="valid_f1">Réserver</button>
			</form>
END;
					}

				}else{

					if ($this->tableau['nomReserveur'] != NULL && isset($_SESSION['userid']) && $this->tableau['user_id']==$_SESSION['userid']){
					$code = $code.'<br>est reservé par '.$this->tableau['nomReserveur'].'<br>';
					$code =  $code . 'Message : ' . $this->tableau['message'];
					}
				}
			}else{
				$montant=Item::compterCagnotte($this->tableau['id']);
				$id=$this->tableau['id'];
				if((isset($_COOKIE["userid"]) && $_COOKIE["userid"]!=$idcreateur )|| !isset($_COOKIE['userid'])){
					if($montant<$this->tableau['montant']){
					$reste=$this->tableau['montant']-$montant;
					$code=$code."<p>Il est encore possible de participer à la cagnotte à hauteur de ".$reste." euros</p>";
					$code=$code.<<<END
					<form id = "f_addCagnotte" method="post" action = "../ajouterCagnotte/$id">
					<label for="f_name">nom:</label>
					<input type="text" id="f_name" name="name" value="$user" required>
					<label for="f_cagnotte">Montant :</label>
					<input type="number" id="f_cagnotte" max=$reste min='1' name="cagnotte" required>
					<button type="submit" name="valider_addCagnotte"value="valid_cagnotte">Valider</button>
					</form>
END;
					}else{
						$code=$code."<p>La cagnotte est pleine</p>";
					}
				}else{
					if($montant<$this->tableau['montant']){
						$code=$code."Cagnotte en cours.";
					}else{
						$code=$code."Cagnotte pleine.";
					}
				}
			}
		}
		$token=$this->tableau['token'];
		$button=<<<END
<form id="retour" method="get" action ="../../afficherliste/$token">
<button type="submit" name="valider_retour" value="valid_retour">Retour à la liste</button>
</form>
END;
		$code=$code.$button;
		$code=$code."</section>";
		return $code;
	}

	private function afficherItemsp(){
		if(isset($_SESSION['userid'])){
		$code= "<section><ul>";
			foreach($this->tableau[0] as $key=>$value){
				$code=$code."<li>".$value['nom']."</li><br>";
				$code=$code.'<img src="../img/'.$value['img'].'"width = "150" height="150"></img><br>';
			}
			$code=$code."</ul></section>";
		}
		return $code;
	}

	public function genererNav($identation){
		$res="";
		if(isset($_SESSION['userid'])){
			$res="<li><a href=\"$identation
			afficherlistesCreateur\">Mes Listes</a></li><li><a href=\"$identation
			affichermodifcreateur\">Compte</a><a href=\"$identation
			utilisateurs\">Liste des utilisateurs</a></li><li><a href=\"$identation
affichercrealiste\">Créer une liste</a></li>";
		}
		$res=$res."<li><a href=\"$identation
			afficherlistespubliques\">Listes publiques</a></li>";
		return $res;
	}

	public function render($int){
		$indentation="";
		switch($int){
			case 1: {
				$content=$this->afficherListes();
				break;
			}

			case 2:{
				$indentation="../";
				$content=$this->afficherListe();
				break;
			}

			case 3:{
				$indentation="../../";
				$content=$this->afficherItem();
				break;
			}
			case 4:{
				$content=$this->afficherItemsp();
				break;
			}

		}
		$res=$this->afficherBoutons($indentation);
		$nav = $this->genererNav($indentation);

		$code= <<<END
<!DOCTYPE html>
<html lang="fr">
<head>
<title>mywishlist</title>
<link rel="shortcut icon" href="../$indentation/img/favicon.ico">
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="stylesheet" href="../$indentation
test.css" />
</head>
<body>
<div id="userpanel">
<ul>
$res
</div>
<section class="head">

<h2>My WishList</h2>
</section>
<!-- Barre de navigation-->
<div class="redline"></div>
<nav>
<ul class="nav1"> <li><a href="$indentation
home">Accueil</a></li>
$nav
</ul>
</nav>
<div class="redline"></div>
$content
<footer>
<ul class="footer"> <li class="footer"><a href="contact.html">Contact</a></li></ul>
</footer>
</body>
</html>

END;
	echo $code;


	}

}
