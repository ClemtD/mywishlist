<?php
require_once 'vendor/autoload.php' ;
use \mywishlist\models\Liste as Liste;
use \mywishlist\models\Item;
use \mywishlist\controleur\ControleurParticipant;
use \mywishlist\controleur\ControleurCreateur;
use \Illuminate\Database\Capsule\Manager as DB;

$db=new DB();
$db->addConnection(parse_ini_file('./src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();
session_start();
$app = new \Slim\Slim() ;

$app->get('/afficherlistes',function(){
	$control=new ControleurParticipant();
	$control->listerListes();
});

$app->get('/afficherliste/:num',function($num){
	$control=new ControleurParticipant();
	$control->afficherListe($num);
});

$app->get('/afficheritem/:num',function($num){
	$control=new ControleurParticipant();
	$control->afficherItem($num);
});

$app->get('/afficherliste/afficheritem/:num',function($num){
	$control=new ControleurParticipant();
	$control->afficherItem($num);
});

$app->get('/affichercrealiste',function(){
	$control=new ControleurCreateur();
	$control->creerAffListe();
});

$app->post('/creerliste',function(){
	$control=new ControleurCreateur();
	$control->creerListe($_POST['nom'],$_POST['date'],$_POST['desc']);
});

$app->get('/afficherlistesCreateur',function(){
	$control=new ControleurCreateur();
	$control->afficherListes();
});

$app->get('/inscription',function(){
	$control=new ControleurCreateur();
	$control->creerAffInscription();
});

$app->post('/validerInscription',function(){
	$control=new ControleurCreateur();
	$control->creerUser($_POST['nom'],$_POST['prenom'],$_POST['email'],$_POST['mdp']);
});

$app->get('/ajouteritem',function(){
	$control=new ControleurCreateur();
	$control->affajouterItem();
});

$app->post('/valideritem',function(){
	$control=new ControleurCreateur();
	$control->creerItem($_POST['nom'],$_POST['descr'],$_POST['prix'],$_POST['url'],$_POST['img'],$_POST['liste_id']);
});

$app->get('/home',function(){
	$control=new ControleurCreateur();
	$control->afficherAccueil();
});

$app->get('/connexion',function(){
	$control=new ControleurCreateur();
	$control->afficherConnexion();
});

$app->post('/verifierconnexion',function(){
	$control=new ControleurCreateur();
	$control->connectionUser($_POST['email'],$_POST['mdp']);
});

$app->get('/deconnexion',function(){
	$control=new ControleurCreateur();
	$control->afficherDeconnexion();
});

$app->post('/afficheritem/reserver/:num',function($num){
	$control=new ControleurParticipant();
	$control->afficherItem($num);
});

$app->get('/afficherlisteCrea/:num',function($num){
	$control=new ControleurCreateur();
	$control->afficherListe($num);
});

$app->get('/affichermodifcreateur',function(){
	$control=new ControleurCreateur();
	$control->creerAffModifCreateur();
});

$app->post('/validermodifuser',function(){
	$control=new ControleurCreateur();
	$control->modifCreateur($_POST['nom'],$_POST['prenom'],$_POST['amdp'],$_POST['nmdp']);
});

$app->get('/afficheritemcrea/:num',function($num){
	$control=new ControleurCreateur();
	$control->afficherItem($num);
});

$app->get('/affichermodifliste/:num',function($num){
	$control=new ControleurCreateur();
	$control->creerAffModifListe($num);
});

$app->post('/validermodifliste/:num',function($num){
	$control=new ControleurCreateur();
	$control->modifListe($_POST['titre'],$_POST['desc'],$num,$_POST['date']);
});

$app->get('/affajouteritem/:num',function($num){
	$control=new ControleurCreateur();
	$control->creerAffAjouterItem($num);
});

$app->post('/valideritem/:num',function($num){
	$control=new ControleurCreateur();
	$control->ajouterItem($num,$_POST['nom'],$_POST['descr'],$_POST['prix'],$_POST['url'],$_FILES['img']['tmp_name']);
});

$app->get('/affichermodifitem/:num',function($num){
	$control=new ControleurCreateur();
	$control->afficherModifItem($num);
});

$app->post('/validermodifitem/:num',function($num){
	$control=new ControleurCreateur();
	$control->modifierItem($num,$_POST['nom'],$_POST['desc'],$_POST['prix'],$_POST['url'],$_FILES['img']['tmp_name']);
});

$app->get('/supprimeritem/:num',function($num){
	$control=new ControleurCreateur();
	$control->supprimerItem($num);
});

$app->post('/supprimerimg/:num',function($num){
	$control=new ControleurCreateur();
	$control->supprimerImg($num);
});

$app->post('/rendrepublique/:num',function($num){
	$control=new ControleurCreateur();
	$control->rendrePublique($num);
});

$app->post('/rendreprivee/:num',function($num){
	$control=new ControleurCreateur();
	$control->rendrePrivee($num);
});

///pour ecrire un message sur une liste
$app->post('/afficherliste/:num',function($num){
	$control=new ControleurParticipant();
	$control->afficherListe($num);
});

$app->get('/afficherlistespubliques',function(){
	$control=new ControleurCreateur();
	$control->afficherListesPubliques();
});

$app->get('/afficherlistepublique/:num',function($num){
	$control=new ControleurCreateur();
	$control->afficherListePublique($num);
});

$app->get('/afficheritemp',function(){
	$control = new ControleurParticipant();
	$control->afficheritemp();
});

$app->get('/partagerliste/:id',function($id){
	$control=new ControleurCreateur();
	$control->partagerListe($id);
});

$app->get('/utilisateurs',function(){
	$control=new ControleurCreateur();
	$control->consulterUtilisateurs();
});

$app->post('/demanderconfirmation',function(){
	$control=new ControleurCreateur();
	$control->confirmer();
});

$app->post('/supprimerCompte',function(){
	$control=new ControleurCreateur();
	$control->supprimerCompte();
});

$app->post('/supprimerCompte',function(){
	$control=new ControleurCreateur();
	$control->supprimerCompte();
});

$app->post('/creerCagnotte/:id',function($id){
	$control=new ControleurCreateur();
	$control->creerCagnotte($id);
});

$app->post('/afficherliste/ajouterCagnotte/:id',function($id){
	$control=new ControleurParticipant();
	$control->ajouterCagnotte($_POST['cagnotte'],$_POST['name'],$id);
});

$app->get('/contact',function(){
	$control=new ControleurCreateur();
	$control->pageContact();
});

$app->run();
